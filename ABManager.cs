﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;


public enum ABtype
{
    //AD_AGRESSION = 0,
    //LEVEL_ORDER = 1,
    //KNIFE_BEHAVIOUR = 2,
    //SKIP_OPTION = 3,
    //FOCUS_SPEED = 4,
    First_Ad = 0//rumman
}

public static class ABManager 
{
    private static void Init()
    {
        _allSettings = new Dictionary<ABtype,HardAB>();

        //_allSettings.Add(ABtype.AD_AGRESSION, new HardAB("peel_ad_intensity_type"));
        //_allSettings.Add(ABtype.LEVEL_ORDER, new HardAB("level_order"));
        //_allSettings.Add(ABtype.KNIFE_BEHAVIOUR, new HardAB("knife"));
        //_allSettings.Add(ABtype.SKIP_OPTION, new HardAB("skip_option"));
        //_allSettings.Add(ABtype.FOCUS_SPEED, new HardAB("focus_speed"));
        _allSettings.Add(ABtype.First_Ad, new HardAB("first_ad"));
    }


    private static Dictionary<ABtype,HardAB> _allSettings;
    public static Dictionary<ABtype, HardAB> allSettings
    {
        get
        {
            if (_allSettings == null) Init();
            return _allSettings;
        }
    }

    public static int GetValue(ABtype type)
    {
        return allSettings[type].GetValue();
    }


    public class HardAB
    {
        string id;
        string key;
        HardData<int> data;

        public HardAB(string id, int editorDefaultValue = -1)
        {
            this.id = id;
            key = string.Format("AB_KEY_{0}", id);
#if UNITY_EDITOR
            data = new HardData<int>(key, editorDefaultValue);
#else
            data = new HardData<int>(key, -1);
#endif
        }


        public string GetID()
        {
            return id;
        }
        public void Assign_IfUnassigned(int i)
        {
            if (data.value == -1)
            {
                data.value = i;
                if (data.value != -1) AnalyticsAssistant.LogABTesting(id, data.value);
            }
        }

        public int GetValue()
        {
            if (data.value <= -1)
            {
                data.value = 0;
                AnalyticsAssistant.LogABTesting(id, data.value);
                Debug.LogFormat("AB value was set to default for key <{0}>", id);
            }
            return data.value;
        }

    }


}
