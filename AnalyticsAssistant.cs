﻿using FRIA;
using System.Collections.Generic;
using UnityEngine;

public static class AnalyticsAssistant 
{

#if UNITY_EDITOR
    static bool logToConsole = false;
#else
    static bool logToConsole = false;
#endif

    public static void LevelStarted(int levelNo,string levelName, string levelType)
    {
        if (logToConsole) Debug.LogFormat("Started Level {2}{0}.{1}", levelNo, levelName,levelType);
        //FacebookManager.LogLevelStarted(levelNo, levelName, ToolSelectionManager.mode.ToString(), levelType); 
    }
    public static void LevelCompleted(int levelNo, string levelName, string levelType)
    {
        if (logToConsole) Debug.LogFormat("Completed Level {2}{0}.{1}", levelNo,levelName, levelType);
        //FacebookManager.LogLevelCompleted(levelNo, levelName, ToolSelectionManager.mode.ToString(), levelType);
        //FacebookManager.LogLevelSkipOrCompletedFull(levelNo, levelName, ToolSelectionManager.mode.ToString(),"3Stars", levelType);
    }

    public static void LevelSkipped(int levelNo, string levelName, string levelType)
    {
        if (logToConsole) Debug.LogFormat("Skipped Level {2}{0}.{1}", levelNo, levelName, levelType);
        //FacebookManager.LogLevelSkipped(levelNo, levelName, ToolSelectionManager.mode.ToString(), levelType);
        //FacebookManager.LogLevelSkipOrCompletedFull(levelNo, levelName, ToolSelectionManager.mode.ToString(), "Skip", levelType);

        HardData<bool> hd = new HardData<bool>("Ad_SKIPPED_ONE_TIME_EVENT_FIRED", false);
        if (hd.value == false)
        {
            //AppsFlyer.trackRichEvent("level_skipped", new Dictionary<string, string>());
            Debug.Log("<color='magenta'>level skip analytics method</color>");
            hd.value = true;
        }
    }
    public static void LevelCompletedAppsFlyer(int lvNow)
    {
        if (lvNow >= 20)
        {
            if(HasItBeenFired(20) == false && lvNow >= 20)
            {
                LevelCompletedOneTimeFire(20);
            }

            if (HasItBeenFired(50) == false && lvNow >= 50)
            {
                LevelCompletedOneTimeFire(50);
            }

            if (HasItBeenFired(75) == false && lvNow >= 75)
            {
                LevelCompletedOneTimeFire(75);
            }

            if (HasItBeenFired(100) == false && lvNow >= 100)
            {
                LevelCompletedOneTimeFire(100);
            }
        }
    }

    public static void ToolUnlocked(int serialNo, string toolName, int combinedLevelNo)
    {
        if (logToConsole) Debug.LogFormat("Tool unlocked {0}.{1} after completing {2} levels", serialNo, toolName, combinedLevelNo);
        //FacebookManager.LogToolUnlocked(serialNo, toolName, combinedLevelNo);
    }
    public static void RewardADClicked(int levelNo, string levelType)
    {
        if (logToConsole) Debug.LogFormat("Reward AD watched at {0}_{1}",levelType,levelNo); 
        //FacebookManager.LogRewardADClicked(levelNo,levelType);
    }

    public static void LogRewardedVideoAdStart(int levelnumber)
    {
        //FacebookManager.LogRewardedVideoAdStart(levelnumber);
    }

    public static void LogRewardedVideoAdComplete(int levelnumber)
    {
        //FacebookManager.LogRewardedVideoAdComplete(levelnumber);   
    }

    static bool HasItBeenFired(int lvNum)
    {
        HardData<bool> hd = new HardData<bool>("LEVEL_COMPLETED_ONE_TIME_EVENT_FIRED_" + lvNum, false);
        return hd.value;
    }

    static void LevelCompletedOneTimeFire(int lvNum)
    {
        HardData<bool> hd = new HardData<bool>("LEVEL_COMPLETED_ONE_TIME_EVENT_FIRED_" + lvNum, false);
        if (hd.value == false)
        {
            //AppsFlyer.trackRichEvent("level_achieved_" + lvNum, new Dictionary<string, string>());
            Debug.Log("<color='magenta'>level achieved num: " + lvNum + "</color>");
            hd.value = true;
        }
    }

    public static void ObjectSelected(string objectName)
    {
        if (logToConsole) Debug.LogFormat("Selected {0}", objectName);
        //FacebookManager.LogObjectSelected(objectName);
    }
    public static void SpeedChanged(string speedLevelText)
    {
        //if (logToConsole)
            Debug.LogFormat("Speed level was changed to {0}",speedLevelText);
        //FacebookManager.LogSpeedChanged(speedLevelText);
    }

    public static void LogABTesting(string abType, int abValue)
    {

        if (logToConsole) Debug.LogFormat("AB value choice made <{0}> {1}", abType, abValue);
        //FacebookManager.LogABTesting(abType, abValue);

    }

    public static void LogPurchase(string currencyCode, string amount)
    {
        System.Collections.Generic.Dictionary<string, string> purchaseEvent = new
System.Collections.Generic.Dictionary<string, string>();
        purchaseEvent.Add("af_currency", currencyCode);
        purchaseEvent.Add("af_revenue", amount);
        purchaseEvent.Add("af_quantity", "1");
        //AppsFlyer.trackRichEvent("af_purchase", purchaseEvent);

        Debug.Log("<color='magenta'>a_f purchase analytics method</color>");

    }
}
