﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FRIA
{

    public class EditorUtilities
    {
        [UnityEditor.MenuItem("Clean/All Player Prefs")]
        public static void ClearPP()
        {
            PlayerPrefs.DeleteAll();
        }
        
        [UnityEditor.MenuItem("Clean/BMAD PData/Goals")]
        public static void ClearGoalProgress()

        {
            PlayerPrefs.DeleteKey("GOALSET_INDEX");
            PlayerPrefs.DeleteKey("GOAL_STATE_0");
            PlayerPrefs.DeleteKey("GOAL_STATE_1");
            PlayerPrefs.DeleteKey("GOAL_STATE_2");
        }

        [UnityEditor.MenuItem("Clean/Daily Reward Timer")]
        public static void ClearDailyRewardTimer()

        {
            PlayerPrefs.DeleteKey("LAST_CLAIM_DATE_DAILYREWARD");
        }
    }
}