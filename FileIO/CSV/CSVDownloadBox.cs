﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
[CreateAssetMenu(fileName = "CSVBox", menuName ="CSV DownLoad Box", order =1)]
public class CSVDownloadBox : ScriptableObject
{

    public string baseDirectory;
    public List<CSVDownloadData> dataList;

    private const string head = "https://docs.google.com/spreadsheets/d/";
    private const string tail = "/export?format=csv";

#if UNITY_EDITOR
    public void Download(int i)
    {
        CSVDownloadData data =  dataList[i];
        string url = string.Format("{0}{1}{2}", head ,data.googleID,tail);
        string save_path = string.Format( "Assets/{0}/{1}.csv",baseDirectory,data.title);

        FRIA.CSVDownloader.DownloadCSV(url,save_path);
    }
#endif
}

