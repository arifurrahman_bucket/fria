﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
#endif


[System.Serializable]
public class CSVDownloadData
{
    public string title;
    public string googleID;
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(CSVDownloadData))]
public class CSVDownloadDataEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        float elementHeight = (position.height - 10) / 2;
        // Calculate rects
        var amountRect = new Rect(position.x, position.y, position.width, elementHeight) ;
        var nameRect = new Rect(position.x, position.y + elementHeight + 5, position.width, elementHeight);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("title"), GUIContent.none);
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("googleID"), GUIContent.none);

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;


        EditorGUI.EndProperty();

    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label)*2 + 10;
    }

}
#endif