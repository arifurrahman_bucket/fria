﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragReporter : MonoBehaviour
{
    const float defWidth = 900;
    public float defPixCount = 15;
    public event System.Action<Drag> OnDragStart;
    public event System.Action<Vector2> OnDrag;
    float minimalPixelCount;

    public static DragReporter instance;
    Drag currentDrag = null;
    private void Start()
    {
        instance = this;
        minimalPixelCount =  (defPixCount/defWidth)* Screen.width;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentDrag = new Drag(new Vector2(Input.mousePosition.x, Input.mousePosition.y), minimalPixelCount);
            OnDragStart?.Invoke(currentDrag);
        }
        else if (Input.GetMouseButton(0))
        {
            Vector2 travelVec = currentDrag.CalculateDragAmount(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            OnDrag?.Invoke(travelVec);
        }
        else 
        { 
            currentDrag = null;
        }
    }
}

public class Drag
{
    Vector2 startPos;
    Vector2 lastPos;
    public Vector2 travelVec { get { return lastPos - startPos; } }
    float startTime;
    float minPixel = 100;

    public event System.Action<Vector2> onDrag;
    public Drag(Vector2 startPosition, float minPix)
    {
        this.minPixel = minPix;
        startTime = Time.time;
        startPos = startPosition;
        lastPos = startPosition;
    }

    public Vector2 CalculateDragAmount(Vector2 currentPosition)
    {
        lastPos = currentPosition;
        Vector2 retVal = Vector2.zero;
        if (travelVec.magnitude >= minPixel)
        {
            retVal = travelVec;
        }
        onDrag?.Invoke(retVal);
        return retVal;
    }
}
