﻿using System.Collections;
using System.Collections.Generic;
using FRIA;
using UnityEngine;


public class CameraAndToolVisibilityController : MonoBehaviour
{
    public const ToolBehaviourType behaviourType = ToolBehaviourType.SingleEdge;

    public const string INTERACTABLE_MESH_LAYER_NAME = "Editable";
    public static int interactableLayer { get { return LayerMask.NameToLayer(INTERACTABLE_MESH_LAYER_NAME); } }
    public static LayerMask interactableLayerMask { get { return LayerMask.GetMask(INTERACTABLE_MESH_LAYER_NAME); } }

    public static event System.Action<RaycastHit, VertexOpMode, Ray> onRayHittingInteractableLayer;


    public Camera cam;

    public int knifeABMode
    {
        get
        {
            return 1;//ABManager.GetValue(ABtype.KNIFE_BEHAVIOUR); 
        }
    }
    //public GameObject knifeRenderer0;
    //public GameObject knifeRenderer1;
    //public GameObject peelerRenderer;
    public Animator hoverAnim;


    public Transform createRoot;
    public Transform targetTrans;
    public Transform visualTrans;
    public float perFrameLerpRate_position = 5;
    public float perFrameLerpRate_rotation = 2;

    //float radiusGrowth = 0.02f;
    //int radiusGrowthIteration = 5;
    float sphereRad = 0.1f;

    void UpdateToolVisual(bool didHit)
    {
        if(hoverAnim)
        switch (behaviourType)
        {
            case ToolBehaviourType.Clawing:
            case ToolBehaviourType.Vertical:
                hoverAnim.SetBool("hover1", !didHit);
                hoverAnim.SetBool("hover2", false);
                break;
            case ToolBehaviourType.SingleEdge:
                hoverAnim.SetBool("hover1", false);
                hoverAnim.SetBool("hover2", !didHit);
                break;
        }
    }

    [Header("Cam Control")]
    public Transform camTrans;
    Vector3 posTarget;
    Quaternion rotTarget;
    public Transform camTrans_Initial;
    public Transform camTrans_Neutral;
    public Transform camTrans_Positive;
    public Transform camTrans_Negative;
    public float posLerpRate = 1;
    public float locLerpRate = 1;
    public float rateMultiplier = 1;


    private void Awake()
    {
        SetNewTargets(camTrans_Initial, 10000);
        TransitionInputReciever.initialInputRecieved += InitialInputCamSetup;
    }
    private void OnDestroy()
    {
        TransitionInputReciever.initialInputRecieved -= InitialInputCamSetup;
    }

    public static bool firstLoad = true;

    private void InitialInputCamSetup()
    {
        SetNewTargets(0, 3);
    }
    private void CamPosReset(Theme theme)
    {
        if (firstLoad)
        {
            SetNewTargets(camTrans_Initial, 10000);
            firstLoad = false;
        }
        else
        {
            SetNewTargets(0, 3);
        }
    }
    void Update()
    {
        //knife visual control
        visualTrans.position = Vector3.Lerp(visualTrans.position, targetTrans.position, perFrameLerpRate_position * Time.deltaTime);
        visualTrans.rotation = Quaternion.Lerp(visualTrans.rotation, targetTrans.rotation, perFrameLerpRate_rotation * Time.deltaTime);


        //camera control
        camTrans.localPosition = Vector3.Lerp(camTrans.localPosition, posTarget, rateMultiplier * posLerpRate * Time.deltaTime);
        camTrans.localRotation = Quaternion.Lerp(camTrans.localRotation, rotTarget, rateMultiplier * locLerpRate * Time.deltaTime);

    }





    public void KnifeMoveUpdate(Vector2 mousePos)
    {
        savedMousePos = mousePos;
        Ray ray = cam.ScreenPointToRay(mousePos);
        VertexOpMode opMode;
        RaycastHit rch;
        if (GetRayCastResult(ray, mousePos, out rch, out opMode))
        {
            UpdateToolVisual(true);
            float angleVal = Mathf.Clamp((Vector3.Angle(rch.normal, Vector3.down) / 90 - 1), -1, 1);
            SetNewTargets(angleVal, 1);
            onRayHittingInteractableLayer?.Invoke(rch, opMode, ray);
            KnifePosUpdate(rch);

        }
        else
        {
            UpdateToolVisual(false);

            KnifeFailedToHitUpdate(mousePos);
            //MeshPeeler.instance.Eject("Ray cast missed", true);


        }



    }
    internal Vector2 savedMousePos;
    public void NoKnifeUpdate()
    {
        UpdateToolVisual(false);
        //MeshPeeler.instance?.Eject("No Input", true);

        targetTrans.position = cam.ScreenToWorldPoint(new Vector3(savedMousePos.x, savedMousePos.y, 2.75f));
        SetFailRot();


    }


    bool lastUpdateWasAMiss = true;
    Vector3 lastHitPoint;
    public void KnifePosUpdate(RaycastHit hit)
    {
        targetTrans.position = hit.point;// + hit.normal * .0001f;
        //if(peeler.)
        if (lastUpdateWasAMiss || (behaviourType == ToolBehaviourType.Vertical))
            targetTrans.LookAt(this.transform.position + hit.normal);
        else
            targetTrans.LookAt(this.transform.position + hit.normal, lastHitPoint - hit.point);// -peeler.GetTravelDirectionFromPeelBuilder());

        lastHitPoint = hit.point;
        lastUpdateWasAMiss = false;
    }
    public void KnifeFailedToHitUpdate(Vector2 mousePos)
    {
        lastUpdateWasAMiss = true;
        targetTrans.position = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 2.75f));
        SetFailRot();

    }

    public void SetFailRot()
    {
        switch (behaviourType)
        {
            case ToolBehaviourType.Clawing:
            case ToolBehaviourType.Vertical:
                targetTrans.LookAt(cam.transform);
                break;
            case ToolBehaviourType.SingleEdge:
                targetTrans.LookAt(cam.transform, cam.transform.right);
                break;
        }
    }


    public bool GetRayCastResult(Ray ray, Vector2 touchPoint, out RaycastHit rch, out VertexOpMode opMode)
    {
        opMode = VertexOpMode.SPHERE;
        if (Physics.Raycast(ray, out rch, 100, layerMask: interactableLayerMask))
        {
            return true;
        }
        else
        {

            if (Physics.SphereCast(ray, sphereRad, out rch, 100, layerMask: interactableLayerMask))
            {
                opMode = VertexOpMode.TUNNEL;
                return true;
            }
            else
            {
                return false;
            }
        }
    }



    public void SetNewTargets(float fraction, float rateMult)
    {
        //Debug.Log("F: " + fraction);
        if (fraction >= 0)
        {
            posTarget = Vector3.Lerp(camTrans_Neutral.localPosition, camTrans_Positive.localPosition, fraction);
            rotTarget = Quaternion.Lerp(camTrans_Neutral.localRotation, camTrans_Positive.localRotation, fraction);
        }
        else
        {
            fraction = -fraction;
            posTarget = Vector3.Lerp(camTrans_Neutral.localPosition, camTrans_Negative.localPosition, fraction);
            rotTarget = Quaternion.Lerp(camTrans_Neutral.localRotation, camTrans_Negative.localRotation, fraction);
        }

        rateMultiplier = rateMult;

    }

    public void SetNewTargets(Transform targetTrans, float rateMult)
    {
        // Debug.Log("T: " + targetTrans);
        posTarget = targetTrans.localPosition;
        rotTarget = targetTrans.localRotation;

        rateMultiplier = rateMult;

    }
}
public enum KnifeOutOfAreaMode
{
    Disappear = 0,
    LastPosition = 1,
    Follow = 2,

}

public enum ToolBehaviourType
{
    SingleEdge = 0,//movement oriented motion
    Vertical = 1,//vertical
    Clawing = 2,//clawing motion
}

public enum VertexOpMode
{
    SPHERE,
    TUNNEL,
}
