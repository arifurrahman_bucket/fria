﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InputController_LaggingEdgePointerType : MonoBehaviour
{
    public RectTransform knifeEdgeTrans;
    public CameraAndToolVisibilityController knifeModelControllerRef;


    public float maxSpeedLimit;
    //float functionalSpeedLimit;

    RectTransform rectTrans;

    Vector2 scaledExtraOffsetForCuttingEdge;
    Vector2 canvasCentreDifference;

    public CanvasScaler canvScaler;

    //Vector2 reverseScaleVec;
    float reverseScaleFactor;
    //public RectTransform testrect;


    void Start()
    {
        rectTrans = this.transform.GetComponent<RectTransform>();

        reverseScaleFactor = canvScaler.referenceResolution.y / Screen.height;

        //reverseScaleVec = Vector2.Scale(canvScaler.referenceResolution, new Vector2(1.0f / Screen.width, 1.0f / Screen.height));
        canvasCentreDifference = new Vector2(Screen.width, Screen.height) * reverseScaleFactor / 2;

        scaledExtraOffsetForCuttingEdge = new Vector2(0, canvScaler.referenceResolution.y / 8.0f);
        knifeEdgeTrans.anchoredPosition = scaledExtraOffsetForCuttingEdge;

        knifeModelControllerRef.savedMousePos = canvasCentreDifference / reverseScaleFactor;
        //Debug.Log(testrect.);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Break();
        }
        if (Input.GetMouseButtonDown(0) && TriggerAreaDetector.pressedOn)
        {
            UpdateKnifePosTarget();
            WalkTheKnife();
        }
        else if (Input.GetMouseButton(0) && TriggerAreaDetector.pressedOn)
        {
            UpdateKnifePosTarget();
            WalkTheKnife(maxSpeedLimit);
        }

        else
        {
            knifeEdgeTrans.gameObject.SetActive(false);
            knifeModelControllerRef.NoKnifeUpdate();
        }



    }

    Vector2 targetPos;
    Vector2 targetMousePos;
    void UpdateKnifePosTarget()
    {
        targetPos = (Vector2)Input.mousePosition * reverseScaleFactor - canvasCentreDifference;
    }


    void WalkTheKnife(float maxSpeed = float.MaxValue)
    {
        knifeEdgeTrans.gameObject.SetActive(true);
        float maxDist = maxSpeed * Time.deltaTime;

        Vector2 walkVec = targetPos - rectTrans.anchoredPosition;

        if (walkVec.magnitude > maxDist)
        {
            rectTrans.anchoredPosition = rectTrans.anchoredPosition + walkVec.normalized * maxDist;
        }
        else
        {
            rectTrans.anchoredPosition = rectTrans.anchoredPosition + walkVec;
        }

        targetMousePos = (rectTrans.anchoredPosition + knifeEdgeTrans.anchoredPosition + canvasCentreDifference) / reverseScaleFactor;

        knifeModelControllerRef.KnifeMoveUpdate(targetMousePos);
    }
}