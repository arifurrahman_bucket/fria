﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(MeshRenderer))]
public class ColorLoaderMeshRenderer : MonoBehaviour
{
    public static IThemeScript startThemeKeep;
    private MeshRenderer rend;
    public int colorID = 0;
    public int materialIndex = 0;
    private void Start()
    {

        Theme theme = Theme.BLUE;
        if (startThemeKeep == null)
        {
            GameObject pref = PrimaryLoader.lastLoadedLevelResource;
            if (pref != null)
            {
                startThemeKeep = pref.GetComponent<IThemeScript>();
            }
        }
        if (startThemeKeep != null)
        {
            theme = startThemeKeep.GetTheme();
        }

        rend = GetComponent<MeshRenderer>();
        LevelInstanceManager.newThemeLoading += OnNewLoad;
        OnNewLoad(theme);
    }
    private void OnNewLoad(Theme theme)
    {
        if (rend != null)
        {
            rend.materials[materialIndex].color = ColorKeeper.GetTheme(theme).colors[colorID];
        }
    }

    private void OnDestroy()
    {
        LevelInstanceManager.newThemeLoading -= OnNewLoad;
    }
}

